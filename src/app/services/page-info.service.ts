import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageInfoService {

  info: any = {};
  team: any[] = [];

  constructor(private http: HttpClient) { 
   this.loadInfo();
   this.loadTeam();
  }

  private loadInfo() {
    this.http.get('assets/data/site-strings.json')
      .subscribe((res: any) => {
        this.info = res;
    })
  }

  private loadTeam() {
    this.http.get('https://angular-course-e6ebf-default-rtdb.firebaseio.com/team.json')
      .subscribe((res: any) => {
        this.team = res;
    })
  }
}
