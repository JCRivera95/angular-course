import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: any[] = [];

  constructor(private http: HttpClient) { 
    this.loadProducts();
  }

  private loadProducts() {
    this.http.get('https://angular-course-e6ebf-default-rtdb.firebaseio.com/products_idx.json')
      .subscribe((res: any) => {
        this.products = res;        
    })
  }
}
